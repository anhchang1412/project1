## Welcome !!! I'm Pham Huu Anh Quoc 2018-3616
## This is my project for project 1 subject in my university
## It a small Web App for you to improve listening and writing English skill
## To Using it please follow this step below <3
## Hope You Like it

## Install Nodejs

```bash
	please install this https://nodejs.org/en/
```

## Install dependencies

```bash
	npm install
```

## Run App

```bash
	npm run start
```