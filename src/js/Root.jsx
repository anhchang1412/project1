import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import LearningPage from './pages/LearningPage';
import ErrorPage from './pages/ErrorPage';
import ListExercise from './pages/ListExercise';

import PrivateRoute from './PrivateRoute';

const Root = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/list" component={ListExercise} />

      <PrivateRoute
        path="/learning/:name"
        component={LearningPage}
      />

      <Route path="/error" component={ErrorPage} />
    </Switch>
  </BrowserRouter>
);

export default Root;
