import React from 'react';
import { CircularProgress, Grid, Box } from '@material-ui/core';
import firebase from 'firebase';

import ProgressTable from 'components/progress/ProgressTable';
import MyTable from '../components/progress/MyTable';

const ProgressPage = () => {
  const [loading, setLoading] = React.useState(true);
  const userId = firebase.auth().currentUser.uid;

  const [fullData, setFullData] = React.useState({
    init: { name: '', score: '', time: '' },
  });
  const [data, setData] = React.useState([
    { name: '', score: '', time: '' },
  ]);

  React.useEffect(() => {
    const getData = () => {
      const rootRef = firebase
        .database()
        .ref(`user/${userId}/progress`);

      rootRef.once('value', (snapshot) => {
        if (!snapshot.exists()) {
          setLoading(false);
          alert('Bạn chưa hoàn thành bài nào !');
        } else {
          const temp = snapshot.val();
          const testData = [];
          Object.keys(temp).forEach((key) => {
            testData.push({
              name: `${temp[key].name}`.toUpperCase(),
              score: temp[key].score,
              time: temp[key].time,
            });
          });
          setData(testData);
          setFullData(temp);
          setLoading(false);
        }
      });
    };

    getData();
  }, [userId]);

  if (loading) {
    return (
      <Grid justify="center" alignItems="center" container direction="column">
        <Box mt={15}>
          <CircularProgress size={200} color="primary" />
        </Box>
      </Grid>
    );
  }

  return (
    <Grid justify="center" alignItems="center" container direction="column">
      <Box mt={3}>
        <ProgressTable fullData={fullData} />
      </Box>

      <Box mt={2} maxWidth="100%">
        <MyTable data={data} />
      </Box>
    </Grid>
  );
};

export default ProgressPage;
