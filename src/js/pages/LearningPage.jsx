/* eslint-disable no-shadow */
import React from 'react';
import {
  Typography, Grid, Box, Button,
} from '@material-ui/core';
import { Link, useParams } from 'react-router-dom';

import firebase from 'firebase';

import Learning from '../components/learning/Learning';
import StopWatch from '../components/learning/StopWatch';
import Layout from '../components/common/Layout';

const LearningPage = () => {
  const location = useParams();
  const { name } = location;

  const [finish, setFinish] = React.useState(false);
  const [time, setTime] = React.useState(0);
  const [timeData, setTimeData] = React.useState(0);

  const handleFinish = React.useCallback(() => {
    setFinish(true);
  }, []);

  const handleTime = React.useCallback((time) => {
    setTime(time);
  }, []);

  // i'm so fucking dump make components like this, but i'm so lazy to
  // restructuring code :(( fuck me!!!
  const handleTimeListening = React.useCallback((time) => {
    setTimeData(time);
  }, []);

  const saveScore = async (score) => {
    await firebase
      .database()
      .ref(`user/${localStorage.getItem('id')}/progress/`)
      .push({
        time: new Date().toLocaleString(),
        score,
        name,
      });
  };

  if (finish) {
    const scoreHere = Math.round(120 - time / timeData);
    const score = scoreHere >= 30 ? scoreHere : 30;

    saveScore(score);

    return (
      <Grid justify="center" alignItems="center" container direction="column">
        <Box mt={10}>
          <Typography variant="h4">Congratulation !!!</Typography>
        </Box>

        <Box mt={5}>
          <Typography variant="h6" color="primary">
            Your Score is: {score}
          </Typography>
        </Box>

        <Box mt={3}>
          <Link to="/">
            <Button variant="contained" color="primary">
              Trang chủ
            </Button>
          </Link>
        </Box>
        <Box mt={2}>
          <Link to="/progress">
            <Button variant="contained" color="primary">
              Xem tiến trình luyện tập của bạn
            </Button>
          </Link>
        </Box>
      </Grid>
    );
  }
  return (
    <Layout>
      <Box mx={{ lg: 20 }}>
        <Grid container alignItems="center" justify="center" spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h4" display="block">
              Listen and write your answer
            </Typography>
          </Grid>

          <Grid
            item
            xs={12}
            sm={8}
            container
            alignItems="center"
            justify="center"
          >
            <Learning
              handleFinish={handleFinish}
              handleTimeListening={handleTimeListening}
              name={name}
            />
          </Grid>

          <Grid item xs={12} sm={4}>
            <StopWatch finish={finish} handleTime={handleTime} />
          </Grid>
        </Grid>
      </Box>
    </Layout>
  );
};

export default LearningPage;
