import React, { useEffect, useState } from 'react';
import firebase from 'firebase';

import { Grid } from '@material-ui/core';
import ExerciseCard from '../components/common/ExerciseCard';
import Layout from '../components/common/Layout';

const ListExercise = () => {
  const [newData, setNewData] = useState();

  useEffect(() => {
    const getData = async () => {
      const response = await (await firebase.database().ref('data').limitToLast(10).once('value')).val();
      setNewData(response);
    };
    getData();
  }, []);

  return (
    <Layout>
      <Grid container direction="column" spacing={2} alignItems="center" style={{ marginTop: 100, marginBottom: 100 }}>
        {newData && Object.keys(newData).map((id) => (
          <Grid
            item
            style={{
              marginBottom: '10px',
              marginTop: '0',
              minWidth: '50%',
              maxWidth: '800px',
            }}
          >
            <ExerciseCard data={newData[id]} />
          </Grid>
        ))}
      </Grid>
    </Layout>
  );
};

export default ListExercise;
