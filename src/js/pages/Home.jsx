import React from 'react';
import {
  Box, Grid,
} from '@material-ui/core';

import Layout from '../components/common/Layout';
import Banner from '../components/home/Banner';
import Rule from '../components/home/Rule';
import LatestAudio from '../components/home/LatestAudio';

const Home = () => (
  <Layout>
    <Box display="flex" flexDirection="column" alignItems="center">
      <Box height={{ xs: 200, sm: 500 }} width="100%">
        <Banner />
      </Box>

      <Box maxWidth={1400}>
        <Grid container justify="center" direction="row">
          <Grid justify="center" item xs={12} sm={6}>
            <Rule />
          </Grid>

          <LatestAudio />
        </Grid>
      </Box>
    </Box>
  </Layout>
);

export default Home;
