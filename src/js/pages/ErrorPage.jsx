import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { useLocation } from 'react-router-dom';

import Layout from '../components/common/Layout';

const ErrorPage = () => {
  const { search } = useLocation();
  const query = new URLSearchParams(search);

  return (
    <Layout>
      <Grid container justify="center" alignItems="center">
        <Typography variant="h3">
          {query.get('title')}
        </Typography>
      </Grid>
    </Layout>
  );
};

export default ErrorPage;
