/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import React, { useState, useEffect } from 'react';
import { Typography, Box, TextField } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import firebase from 'firebase';

import { Markup } from 'interweave';
import AudioPlayer from './AudioPlayer';

const TextLevel = styled(Typography)({
  color: '#fff',
});

const AnswerInput = styled(Box)(({ theme }) => ({
  borderRadius: theme.shape.borderRadius,
  border: `1px solid ${theme.palette.grey[400]}`,
}));

const Learning = ({ handleFinish, handleTimeListening, name }) => {
  const storageRef = firebase.storage().ref();

  const [title, setTitle] = useState('');
  const [data, setData] = useState('initial data');
  const [link, setLink] = useState('');
  const [json, setJson] = useState('');
  // Set data + audio
  storageRef.child(`audio/${name}.mp3`).getDownloadURL().then((url) => setLink(url));

  useEffect(() => {
    const getData = async () => {
      const response = await (await firebase.database().ref(`/data/${name.toLowerCase()}`).once('value')).val();
      setJson(response);
    };
    getData();
  }, [name]);

  const [limitIndexAnswer, setLimitIndexAnswer] = useState(0);
  const [indexAnswer, setIndexAnswer] = useState(0);
  const [answer, setAnswer] = useState(['xx']);

  const [limitIndex, setLimitIndex] = useState(0);
  const [index, setIndex] = useState(0);
  const [wrongTypeCount, setWrongTypeCount] = useState(0);
  const [wordCurrent, setWordCurrent] = useState('');

  const [answerInput, setAnswerInput] = useState('');
  const [inputWord, setInputWord] = useState('');

  const availableCharacter = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';

  useEffect(() => {
    if (Object.keys(json).includes('listening')) {
      const arrayValue = json.listening[0][2].split(' ');
      setTitle(json.name);
      handleTimeListening(parseInt(json.time, 10));
      setData(json.listening);
      setLimitIndexAnswer(json.listening.length - 1);
      setAnswer(arrayValue);
      setLimitIndex(arrayValue.length - 1);
      setWordCurrent(arrayValue[0].toLowerCase().replace(/[^A-Za-z]/ig, ''));
    }
  }, [handleTimeListening, json]);

  const makeColorForText = (text, type) => {
    if (type === 1) {
      return `<span style="color: #1976d2;font-weight:bold;margin-left: 0.25em;">${text} </span>`;
    }

    return `<span style="color: red;font-weight:bold;margin-left: 0.25em;">${text} </span>`;
  };

  const handleKeyPress = (event) => {
    if (event.keyCode < 65 && event.keyCode > 90) {
      event.preventDefault();
    }
  };

  const handleUpdateField = (event) => {
    const value = event.target.value.toLowerCase();
    const { length } = value;
    const compareWord = wordCurrent.slice(0, length);

    if (value === compareWord) {
      setInputWord(value);

      if (value === wordCurrent) {
        setInputWord('');

        if (wrongTypeCount <= 2) setAnswerInput((prevAnswerInput) => `${prevAnswerInput} ${makeColorForText(answer[index], 1)}`);
        else setAnswerInput((prevAnswerInput) => `${prevAnswerInput} ${makeColorForText(answer[index], 0)}`);

        if (index === limitIndex) {
          if (indexAnswer === limitIndexAnswer) {
            handleFinish();
            return;
          }
          const newData = `${data[indexAnswer + 1][2]}`.split(' ');
          setAnswer(newData);
          setIndexAnswer((prevIndexAnswer) => prevIndexAnswer + 1);
          setLimitIndex(newData.length - 1);
          setWordCurrent(newData[0].replace(/[^A-Za-z]/ig, '').toLowerCase());
          setIndex(0);
        } else {
          setWordCurrent(answer[index + 1].replace(/[^A-Za-z]/ig, '').toLowerCase());
          setIndex((prevIndex) => prevIndex + 1);
        }

        setWrongTypeCount(0);
      }
    } else {
      setWrongTypeCount((prevWrongTypeCount) => prevWrongTypeCount + 1);
    }

    setInputWord((prevInputWord) => prevInputWord);
  };

  return (
    <>
      <Box width="100%" mt={3} height={100} display="flex" alignItems="center">
        <Box height={100} width={100} display="flex" alignItems="center" bgcolor="blue">
          <TextLevel variant="h4" align="center">Level 11</TextLevel>
        </Box>

        <Box ml={2}>
          <Typography variant="h4">{title}</Typography>
          <AudioPlayer
            link={link}
            start={data[indexAnswer][0]}
            end={data[indexAnswer][1]}
          />
        </Box>
      </Box>

      <Box mt={2} width="100%">
        <AnswerInput minHeight={150} p={2}>
          <Markup content={answerInput} />
        </AnswerInput>
      </Box>

      <Box mt={3} width="100%">
        <TextField
          fullWidth
          label="Nhập từ bạn nghe được"
          variant="outlined"
          value={inputWord}
          onKeyDown={handleKeyPress}
          onChange={handleUpdateField}
        />
      </Box>

      <Box mt={3} width="100%">
        <TextField
          fullWidth
          label="Các từ có thể tiếp theo"
          variant="outlined"
          multiline
          rows={2}
          disabled
          value={availableCharacter}
        />
      </Box>
    </>
  );
};

Learning.propTypes = {
  handleFinish: PropTypes.func.isRequired,
  handleTimeListening: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default Learning;
