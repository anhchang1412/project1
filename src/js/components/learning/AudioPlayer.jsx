/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import { IconButton } from '@material-ui/core';
import PlayCircleOutlineRoundedIcon from '@material-ui/icons/PlayCircleOutlineRounded';
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';
import PropTypes from 'prop-types';

const AudioPlayer = ({ link, end, start }) => {
  console.log(link, end, start);
  const [play, setPlay] = React.useState(true);
  const [audioPart, setAudioPart] = React.useState(null);
  const endHere = parseFloat(end);
  const startHere = parseFloat(start);
  const audio = `${link}#t=${start},${end}`;

  const playAudio = () => {
    if (audioPart) {
      audioPart.currentTime = startHere;
      audioPart.play();
    }
  };

  const pauseAudio = () => {
    if (audioPart) {
      audioPart.pause();
    }
  };

  const handlePause = () => {
    if (audioPart && (audioPart.currentTime - endHere) > 0.01) {
      pauseAudio();
    }
  };

  const handlePlay = (check) => {
    if (check === true) {
      setPlay(true);
      playAudio();
    } else if (check === false) {
      setPlay(false);
      pauseAudio();
    } else if (play) {
      playAudio();
    }
    handlePause();
  };

  return (
    <>
      <IconButton>
        {play
          ? <PauseCircleFilledIcon color="primary" fontSize="large" onClick={() => handlePlay(false)} />
          : <PlayCircleOutlineRoundedIcon color="primary" fontSize="large" onClick={() => handlePlay(true)} />}
      </IconButton>
      <audio
        src={audio}
        ref={(element) => { setAudioPart(element); }}
        onPause={() => handlePlay()}
        autoPlay={play}
        onTimeUpdate={handlePause}
      />
    </>
  );
};

AudioPlayer.propTypes = {
  link: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  start: PropTypes.string.isRequired,
};

export default AudioPlayer;
