import React from 'react';
import useStopwatch from 'react-timer-hook';
import { Typography, Box } from '@material-ui/core';

// eslint-disable-next-line react/prop-types
const StopWatch = ({ finish, handleTime }) => {
  const {
    seconds, minutes, hours, pause,
  } = useStopwatch({ autoStart: true });

  if (finish) {
    pause();
    handleTime(hours * 3600 + minutes * 60 + seconds);
  }

  return (
    <Box alignItems="center" display="flex" flexDirection="column">
      <Typography variant="h3">Time</Typography>
      <Typography variant="h4">
        {hours} : {minutes} : {seconds}
      </Typography>
    </Box>
  );
};

export default StopWatch;
