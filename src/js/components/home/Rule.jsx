import React from 'react';
import {
  Box, Grid, Typography, Paper,
} from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';
import { styled } from '@material-ui/core/styles';

const Warning = styled(WarningIcon)({
  color: 'red',
});

const Rule = () => (
  <>
    <Box my={3}>
      <Typography variant="h4" align="center">
        Rules
      </Typography>
    </Box>

    <Box mx={2}>
      <Paper>
        <Box px={2} py={4}>
          <Typography variant="subtitle1">
            Chọn bài tập trong phần LEARNING sau đó chọn Level, có 3
            mức độ cho bạn chọn
            <br />
            Level-1:
            <br />
            +Bạn sẽ chỉ cần nhập 3 chữ cái đầu của 1 từ.
            <br />
            Level-2:
            <br />
            +Phải nhập đủ tất cả chũ cái của 1 từ, thời lượng nghe
            ngắn.
            <br />
            Level-3:
            <br />
            +Thời lượng nghe trong 1 lần {'>'} 15s.
          </Typography>

          <Grid container alignItems="center">
            <Warning fontSize="large" />
            <Typography variant="subtitle1" display="inline">
              Chỉ khi nào bạn nhập đúng thì chữ cái tiếp theo mới hiển
              thị !!!
            </Typography>
          </Grid>
        </Box>
      </Paper>
    </Box>
  </>
);

export default Rule;
