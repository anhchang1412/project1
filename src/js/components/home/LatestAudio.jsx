import React, { useEffect, useState } from 'react';
import {
  Box, Grid, Typography, Button,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import firebase from 'firebase';
import { Link } from 'react-router-dom';

import ExerciseCard from '../common/ExerciseCard';

const GridTop = styled(Grid)({
  marginTop: '5px',
});

const LinkStyled = styled(Link)({
  textDecoration: 'none',
  color: 'Blue',
});

const LatestAudio = () => {
  const [newData, setNewData] = useState();

  useEffect(() => {
    const getData = async () => {
      const response = await (await firebase.database().ref('data').limitToLast(2).once('value')).val();
      setNewData(response);
    };
    getData();
  }, []);

  return (
    <Grid container justify="center" item xs={12} sm={6}>
      <Box my={3}>
        <Typography variant="h4">Latest Exercise Audio</Typography>
      </Box>

      <Grid
        container
        spacing={2}
        style={{ maxWidth: '1200px', margin: '0 1em 0 1em' }}
      >
        {newData && Object.keys(newData).map((id) => (
          <GridTop
            item
            xs={12}
            style={{ marginBottom: '10px', marginTop: '0' }}
          >
            <ExerciseCard data={newData[id]} />
          </GridTop>
        ))}
      </Grid>

      <Grid container justify="center">
        <Box my={3}>
          <Button color="primary" size="large">
            <LinkStyled to="/list">Watch more</LinkStyled>
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
};

export default LatestAudio;
