import React from 'react';
import { styled } from '@material-ui/core/styles';
import imgUrl from '../../../assets/img/mainbanner.jpg';

const Img = styled('img')({
  width: '100%',
  maxHeight: '100%',
  objectFit: 'cover',
});

const Banner = () => <Img src={imgUrl} />;

export default Banner;
