import React from 'react';
import {
  Grid, Paper, Typography, CardMedia, Button,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

const ImgCard = styled(CardMedia)({
  width: 'auto',
  height: '150px',
  objectFit: 'contain',
  borderRadius: '10px',
  margin: '0 3px',
});

const GridStyled = styled(Grid)({
  margin: '0 5px',
});

const LinkStyled = styled(Typography)({
  color: '#fff',
  fontWeight: 'bold',
});

const CardItems = ({ data }) => {
  const history = useHistory();
  return (
    <Paper>
      <Grid container spacing={2} style={{ padding: '5px' }}>
        <Grid item xs={12} sm={4}>
          <ImgCard image={data.imgLink} title="hello" />
        </Grid>

        <Grid item xs={12} sm={8} container>
          <GridStyled item xs container direction="column" spacing={2}>
            <Grid item xs>
              <Typography
                gutterBottom
                variant="subtitle1"
                style={{ fontWeight: 'bold' }}
              >
                {`${data.name.toUpperCase()}`}
              </Typography>

              <Typography variant="body2" gutterBottom>
                {data.preview}
              </Typography>

              <Typography variant="body2" color="textSecondary">
                Thời Lượng: {data.time}
              </Typography>
            </Grid>

            <Grid item>
              <Button
                variant="contained"
                style={{ backgroundColor: '#1f88e5' }}
                onClick={() => history.push(`/learning/${data.name}`)}
              >
                <LinkStyled>Listening</LinkStyled>
              </Button>
            </Grid>
          </GridStyled>
        </Grid>
      </Grid>
    </Paper>
  );
};

CardItems.propTypes = {
  data: PropTypes.object.isRequired,
};

export default CardItems;
