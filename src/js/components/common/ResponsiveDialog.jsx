import React from 'react';
import {
  Box, Grid, Dialog, useMediaQuery, IconButton,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { Close } from '@material-ui/icons';
import PropTypes from 'prop-types';

const ResponsiveDialog = ({
  children,
  onClose,
  ...dialogProps
}) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <Dialog fullScreen={fullScreen} onClose={onClose} {...dialogProps}>
      <Box p={3} clone>
        <Grid container justify="flex-end">
          <IconButton><Close onClick={onClose} /></IconButton>
        </Grid>
      </Box>
      {children}
      <Box mt={2} />
    </Dialog>
  );
};

ResponsiveDialog.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ResponsiveDialog;
