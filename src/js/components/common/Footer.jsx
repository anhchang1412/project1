import React from 'react';
import {
  Box,
  Container,
  Grid,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import { Call, EmailOutlined } from '@material-ui/icons';

const CenterGrid = styled(Grid)(({ theme }) => ({
  textAlign: 'center',
  [theme.breakpoints.up('md')]: {
    textAlign: 'initial',
  },
}));

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  minHeight: 0,
  padding: 0,
  justifyContent: 'center',
  [theme.breakpoints.up('md')]: {
    justifyContent: 'flex-start',
  },
}));

const Title = styled(Typography)(() => ({
  color: '#fff',
}));

const Link = styled('a')({
  textDecoration: 'none',
  color: '#fff',
});

const Footer = () => (
  <Box pt={4} style={{ backgroundColor: '#1f88e5' }}>
    <Container>
      <Grid container spacing={4}>
        <CenterGrid item xs={12} md={4}>
          <StyledToolbar>
            <Title variant="h4">BKE-LEARNING</Title>
          </StyledToolbar>

          <Box pt={2} clone>
            <Title variant="h5">Phạm Hữu Anh Quốc</Title>
          </Box>
          <Box pt={2} clone>
            <Typography style={{ color: '#fff' }}>
              Đại Học Bách Khoa Hà Nội
            </Typography>
          </Box>

          <Box pt={2}>
            <Typography style={{ color: '#fff' }}>
              K63 - Khoa Học Máy Tính
            </Typography>
          </Box>
        </CenterGrid>

        <Grid item xs={12} md={4}>
          <Title variant="h4">Liên hệ</Title>

          <ListItem style={{ paddingLeft: 0, paddingRight: 0 }}>
            <ListItemIcon style={{ maxWidth: 30 }}>
              <Call style={{ color: '#fff' }} />
            </ListItemIcon>
            <ListItemText>
              <Link href="tel:+84979980300">0979980300</Link>
            </ListItemText>
          </ListItem>

          <ListItem style={{ paddingLeft: 0, paddingRight: 0 }}>
            <ListItemIcon style={{ maxWidth: 30 }}>
              <EmailOutlined style={{ color: '#fff' }} />
            </ListItemIcon>
            <ListItemText>
              <Link href="mailto:anhchang1412@gmail.com">
                anhchang1412@gmail.com
              </Link>
            </ListItemText>
          </ListItem>

          <ListItem style={{ paddingLeft: 0, paddingRight: 0 }}>
            <ListItemText>
              <Link
                href="/policy"
                target="_blank"
                rel="noopener noreferrer"
              >
                Quy chế hoạt động
              </Link>
            </ListItemText>
          </ListItem>
        </Grid>

        <Grid item xs={12} md={4}>
          <Title variant="h4">Ứng dụng liên quan</Title>
        </Grid>
      </Grid>
    </Container>
  </Box>
);

export default Footer;
