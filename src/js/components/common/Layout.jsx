import React from 'react';
import PropTypes from 'prop-types';

import Header from './header/Header';
import Footer from './Footer';

const Layout = ({ children }) => (
  <>
    <Header />
    <div style={{ minHeight: 'calc(100vh - 200px)' }}>
      {children}
    </div>
    <Footer />
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
