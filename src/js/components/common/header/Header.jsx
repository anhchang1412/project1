import React from 'react';
import { List } from '@material-ui/icons';
import {
  AppBar,
  Toolbar as MuiToolbar,
  IconButton,
  Typography,
  Menu,
  MenuItem,
  Box,
  Container,
  CssBaseline,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';

import { Link } from 'react-router-dom';
import Search from './Search';
import ButtonLogin from './ButtonLogin';

const AppBarStyled = styled(AppBar)({
  backgroundColor: '#1e88e5',
});

const Div = styled('div')({
  flexGrow: 1,
});

const Toolbar = styled(MuiToolbar)({
  padding: '10px 0',
  flexWrap: 'wrap',
});

const LinkStyled = styled(Link)({
  textDecoration: 'none',
  color: 'Black',
});

const Header = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <AppBarStyled position="static">
      <CssBaseline />
      <Container maxWidth="lg">
        <Toolbar>
          <IconButton edge="start" color="inherit" onClick={handleClick}>
            <List fontSize="large" />
          </IconButton>
          <Menu
            open={open}
            anchorEl={anchorEl}
            onClose={() => setAnchorEl(null)}
          >
            <MenuItem onClick={() => setAnchorEl(null)}>
              <LinkStyled to="/list">Luyện Nghe </LinkStyled>
            </MenuItem>
            <MenuItem onClick={() => setAnchorEl(null)}>
              <LinkStyled to="/progress">Tiến Trình Luyện Tập </LinkStyled>
            </MenuItem>
          </Menu>
          <LinkStyled to="/">
            <Typography style={{ color: 'white', fontSize: '1.6rem' }}>
              BKE-LEARNING
            </Typography>
          </LinkStyled>

          <Div />

          <Search />
          <Box ml={{ xs: 3, sm: 5 }} order={{ xs: 2, sm: 3 }}>
            <ButtonLogin />
          </Box>
        </Toolbar>
      </Container>
    </AppBarStyled>
  );
};

export default Header;
