import React, { useState, useCallback } from 'react';
import {
  Button,
  Grid,
  Avatar,
  Typography,
  Menu,
  MenuItem,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import LoginModal from '../../auth/login/LoginModal';

const LinkStyled = styled(Link)({
  textDecoration: 'none',
});

const ButtonLogin = () => {
  const [open, setOpen] = useState(false);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const openLogout = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  if (localStorage.getItem('check')) {
    return (
      <div>
        <Grid container spacing={1} alignItems="center">
          <Grid item>
            <Avatar alt="user-avatar" src="" />
          </Grid>
          <Grid>
            <Menu
              open={openLogout}
              anchorEl={anchorEl}
              onClose={() => setAnchorEl(null)}
            >
              <MenuItem
                onClick={() => {
                  setAnchorEl(null);
                  localStorage.clear();
                }}
              >
                <LinkStyled to="/">Đăng xuất</LinkStyled>
              </MenuItem>

              <MenuItem>
                <LinkStyled to="/update-info">Cập nhật thông tin</LinkStyled>
              </MenuItem>
            </Menu>

            <Typography variant="subtitle1">
              {localStorage.getItem('name')}
            </Typography>

            <Button style={{ padding: 0 }} onClick={handleClick}>
              <Typography style={{ color: 'white' }} variant="caption">
                Tài khoản
              </Typography>
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }

  return (
    <>
      <Button
        variant="contained"
        color="secondary"
        onClick={() => setOpen(true)}
      >
        Login
      </Button>
      <LoginModal open={open} onClose={handleClose} />
    </>
  );
};

export default ButtonLogin;
