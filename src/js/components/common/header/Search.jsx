import React from 'react';
import axios from 'axios';
import { InputBase, Box } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';

import Translate from './Translate';

const SearchBox = styled(Box)(({ theme }) => ({
  [theme.breakpoints.only('xs')]: {
    flexBasis: '100%',
  },
  paddingLeft: '10px',
}));

const Search = () => {
  const [data, setData] = React.useState(null);
  const [keySearch, setKeySearch] = React.useState('');
  const [openTranslate, setOpenTranslate] = React.useState(false);

  const handleClose = () => {
    setOpenTranslate(false);
  };

  const handleSearch = async (e) => {
    if (e.keyCode === 13) {
      const corsUrl = 'https://cors-anywhere.herokuapp.com/';
      const proxyurl = `https://translate-api198.herokuapp.com/api/translate?from=en&to=vi&content=${keySearch}`;
      setOpenTranslate(true);
      setData(null);
      await axios.get(corsUrl + proxyurl)
        .then((res) => {
          setData(res);
        }).catch(() => {
          setOpenTranslate(false);
        });
    }
  };

  return (
    <SearchBox
      order={{ xs: 3, sm: 2 }}
      mt={{ xs: 1, sm: 0 }}
      borderRadius={30}
      bgcolor="background.paper"
      display="flex"
      alignItems="center"
      minWidth={{ xs: 0, md: 350 }}
    >
      <Translate open={openTranslate} onClose={handleClose} data={data} keySearch={keySearch} />

      <InputBase
        fullWidth
        placeholder="Nhập từ cần dịch"
        value={keySearch}
        onChange={(e) => setKeySearch(e.target.value)}
        onKeyDown={handleSearch}
      />
    </SearchBox>
  );
};

export default Search;
