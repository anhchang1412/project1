import React from 'react';
import {
  Typography,
  Box,
  Grid,
  Dialog,
  DialogContent,
  DialogTitle,
  CircularProgress,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Markup } from 'interweave';
import PropTypes from 'prop-types';

const Translate = ({
  open, onClose, data, keySearch,
}) => {
  const renderTranslate = () => {
    if (data === null) {
      return (
        <Box m={5}>
          <Grid container justify="center">
            <CircularProgress size={150} />
          </Grid>
        </Box>
      );
    }

    const { text, pronunciation, more } = data.data.data ? data.data.data : '';

    return (
      <Box mx={3} mt={2} pb={5}>
        <Typography variant="h6" align="center" color="primary">{keySearch} : {text}</Typography>
        <Box mt={2}>
          Phát âm : {pronunciation}
        </Box>
        <Box mt={2}>
          Phân Loại : {more.type}
        </Box>
        <Box mt={2}>
          <Markup content={more.text} />
        </Box>
      </Box>
    );
  };

  return (
    <Dialog onClose={onClose} open={open} maxWidth="sm">
      <DialogTitle>
        <Grid container justify="flex-end">
          <Close onClick={onClose} />
        </Grid>
        <Grid container justify="center">
          <Typography variant="h5" align="center">
            Welcome to our App&lsquo;s Translate
          </Typography>
        </Grid>
      </DialogTitle>
      <DialogContent>
        {renderTranslate()}
      </DialogContent>
    </Dialog>
  );
};

Translate.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  keySearch: PropTypes.string.isRequired,
};

export default Translate;
