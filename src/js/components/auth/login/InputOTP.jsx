import React from 'react';
import {
  Typography, Box, Grid, Button, CircularProgress, Container,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import OTPInput from 'react-otp-input';
import * as firebase from 'firebase';
import Countdown from 'react-countdown-now';

import ResponsiveDialog from '../../common/ResponsiveDialog';
import ShowError from '../../common/ShowError';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));

const InputOTP = ({ open, onClose, verificationId }) => {
  const classes = useStyles();

  const [OTP, setOTP] = React.useState('');
  const [count, setCount] = React.useState(true);
  const [error, setError] = React.useState(false);
  const [date, setDate] = React.useState(Date.now() + 10000);
  const [loading, setLoading] = React.useState(false);

  const handleClose = React.useCallback(() => {
    setError(false);
  }, []);

  const update = async (phone) => {
    await firebase.database().ref('user/').push({
      phoneNumber: phone,
      name: '',
    });
  };

  const checkUser = async () => {
    const rootRef = firebase.database().ref();
    const phone = localStorage.getItem('phoneNumber');

    await rootRef.child('user')
      .orderByChild('phoneNumber')
      .equalTo(phone)
      .once('value', (snapshot) => {
        if (!snapshot.exists()) {
          update(phone);
          window.location.href = '/update-info';
        } else {
          const data = snapshot.val();
          const { name } = data[Object.keys(data)[0]];
          localStorage.setItem('name', name);
          localStorage.setItem('id', Object.keys(data)[0]);
          window.location.href = '/';
        }
      });
  };

  const confirm = () => {
    setLoading(true);
    const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, OTP);
    firebase
      .auth()
      .signInWithCredential(credential)
      .then(() => {
        localStorage.setItem('check', 'true');
        checkUser();
        onClose();
      })
      .catch(() => {
        setError(true);
        setLoading(false);
      });
  };
  const resend = () => {
    setCount(true);
    setDate(Date.now() + 10000);
  };
  return (
    <ResponsiveDialog onClose={onClose} open={open} maxWidth="sm">
      <Container maxWidth="xs">
        <Grid container alignItems="center" justify="center" spacing={3}>
          <Grid item xs={12}>
            <Typography align="left" variant="h6">
              Nhập mã OTP
            </Typography>
          </Grid>
          <Grid item>
            <Box mt={8}>
              <Typography variant="h4" align="center">
                <OTPInput
                  shouldAutoFocus
                  value={OTP}
                  numInputs={6}
                  inputStyle={{
                    fontSize: '1em',
                    margin: '0 1rem',
                    border: '0',
                    borderBottom: 'solid 1px',
                  }}
                  onChange={setOTP}
                />
              </Typography>
            </Box>
          </Grid>
          <Grid item container spacing={3} xs={12}>
            <Grid item xs={9}>
              <Typography align="left" color="textSecondary" variant="subtitle2">
                Không nhận được mã
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Typography
                align="left"
                color={count ? 'textSecondary' : 'primary'}
                variant="subtitle2"
              >
                {count ? (
                  <Countdown
                    date={date}
                    renderer={(props) => <Typography align="right">{props.seconds}</Typography>}
                    onComplete={() => setCount(false)}
                  />
                ) : (
                  <Typography variant="subtitle2" align="right" onClick={resend}>
                    Gửi lại
                  </Typography>
                )}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Box mt={3} mb={3}>
          <Button
            fullWidth
            size="medium"
            variant="contained"
            color="primary"
            disabled={OTP.length !== 6}
            onClick={confirm}
          >
            {loading ? (
              <div className={classes.root}>
                <CircularProgress size={20} color="inherit" />
              </div>
            ) : (
              'Tiếp tục'
            )}
          </Button>
          <ShowError
            open={error}
            onClose={handleClose}
            title="Cảnh báo"
            content="Có lỗi xảy ra! Vui lòng thử lại"
          />
        </Box>
      </Container>
    </ResponsiveDialog>
  );
};

InputOTP.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  verificationId: PropTypes.any.isRequired,
  seconds: PropTypes.string.isRequired,
};

export default InputOTP;
