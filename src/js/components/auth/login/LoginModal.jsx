import React from 'react';
import firebase from 'firebase';
import PropTypes from 'prop-types';

import InputOTP from './InputOTP';
import InputPhoneNumber from './InputPhoneNumber';

const firebaseConfig = {
  apiKey: 'AIzaSyANvCjdCY-snbYWjZmjDSHvBEVwbMREIBs',
  authDomain: 'newproject-7dbc9.firebaseapp.com',
  databaseURL: 'https://newproject-7dbc9.firebaseio.com',
  projectId: 'newproject-7dbc9',
  storageBucket: 'newproject-7dbc9.appspot.com',
  messagingSenderId: '103457727754',
  appId: '1:103457727754:web:6548108c189acc4e8b46f3',
  measurementId: 'G-KBKKP6E27K',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  firebase.auth().languageCode = 'vi';
}

const LoginModal = ({ open, onClose }) => {
  const [step, setStep] = React.useState(1);
  const [verificationId, setVerificationId] = React.useState('');

  switch (step) {
    case 1:
      return (
        <InputPhoneNumber
          setStep={setStep}
          setVerificationId={setVerificationId}
          open={open}
          onClose={onClose}
        />
      );
    case 2:
      return (
        <InputOTP
          verificationId={verificationId}
          open={open}
          onClose={onClose}
        />
      );
    default:
      return null;
  }
};

LoginModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default LoginModal;
