import React from 'react';
import {
  Typography,
  Box,
  Grid,
  Input,
  Button,
  CircularProgress,
  Container,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import * as firebase from 'firebase';
import PropTypes from 'prop-types';

import ResponsiveDialog from '../../common/ResponsiveDialog';
import ShowError from '../../common/ShowError';

const InputPhoneNumber = ({
  open, onClose, setStep, setVerificationId,
}) => {
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const { handleSubmit } = useForm();

  const handleClose = React.useCallback(() => {
    setError(false);
  }, []);

  const handlePhoneNumberSubmit = async () => {
    setLoading(true);
    const recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      size: 'invisible',
    });

    try {
      const confirmationResult = await firebase
        .auth()
        .signInWithPhoneNumber(`+84${phoneNumber.slice(1)}`, recaptchaVerifier);
      setVerificationId(confirmationResult.verificationId);
      localStorage.setItem('phoneNumber', `+84${phoneNumber.slice(1)}`);
      setStep(2);
    } catch (e) {
      setLoading(false);
      setError(true);
    }
  };

  return (
    <ResponsiveDialog open={open} onClose={onClose}>
      <Container maxWidth="xs">
        <form autoComplete="off" onSubmit={handleSubmit(handlePhoneNumberSubmit)}>
          <Box mb={3}>
            <Typography variant="h5" align="left" color="primary">
              Chào bạn!
            </Typography>
          </Box>
          <Box mb={3}>
            <Typography color="textSecondary">Số điện thoại</Typography>
            <Input
              placeholder="0123456789"
              fullWidth
              name="phoneNumber"
              autoFocus
              value={phoneNumber}
              type="tel"
              onChange={(e) => setPhoneNumber(e.target.value)}
            />
          </Box>
          <Grid container alignItems="center" spacing={3}>
            <Grid item xs={12}>
              <Typography align="left" color="textSecondary">
                Mã OTP để xác nhận sẽ được gửi tới số điện thoại của bạn qua tin nhắn
              </Typography>
            </Grid>
          </Grid>
          <Box mt={3} mb={3}>
            <Typography align="center">
              <Button
                fullWidth
                type="submit"
                id="sign-in-button"
                size="medium"
                variant="contained"
                color="primary"
                disabled={phoneNumber.length <= 8}
              >
                {loading ? (
                  <div>
                    <CircularProgress size={20} color="inherit" />
                  </div>
                ) : (
                  'Tiếp tục'
                )}
              </Button>
            </Typography>
          </Box>

          <ShowError
            open={error}
            onClose={handleClose}
            title="Cảnh báo"
            content="Có lỗi xảy ra! Vui lòng kiểm tra lại số  điện thoại"
          />
        </form>
      </Container>
    </ResponsiveDialog>
  );
};

InputPhoneNumber.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  setStep: PropTypes.func.isRequired,
  setVerificationId: PropTypes.func.isRequired,
};

export default InputPhoneNumber;
